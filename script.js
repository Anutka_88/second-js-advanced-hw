const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function booksList(arr) {
  let ulList = document.createElement("ul");
  arr.forEach(element => {
    let li = document.createElement("li");
    li.textContent = `author: ${element.author}; name: ${element.name}; price: ${element.price}`;
    if (element.author != undefined && element.name != undefined && element.price != undefined) {
      ulList.append(li);
    }
    try{
    if (element.author === undefined ) {
 throw new Error('Author is Undefined') 
    } if (element.name === undefined) {
 throw new Error('Name is Undefined') 
    } if (element.price === undefined) {
 throw new Error('Price is Undefined')
    } 
  } catch (error) {
    console.log(`${error.name}: ${error.message}`);
  }
  });
  return ulList;
}
 let booksUl = booksList(books);
 const divCont = document.getElementById("root");
divCont.append(booksUl);
